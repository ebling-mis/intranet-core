# frozen_string_literal: true

require 'intranet/abstract_responder'

module Intranet
  class TestResponder < AbstractResponder
    attr_reader :finalized

    def initialize(responses = {}, hide_from_menu = false)
      @responses = responses
      @finalized = false
      @hide_from_menu = hide_from_menu
    end

    def finalize
      @finalized = true
      super
    end

    def in_menu?
      return false if @hide_from_menu

      super
    end

    def self.module_name
      'test-responder'
    end

    def self.module_version
      '0.0.0'
    end

    def self.module_homepage
      'http://nil/'
    end

    def generate_page(path, query)
      if path.start_with?('/query')
        [200, 'text/plain', dump_with_encoding(path, query)]
      else
        @responses.fetch(path)
      end
    rescue KeyError
      super(path, query)
    end

    private

    def dump_with_encoding(path, query)
      "PATH=#{path} (#{path.encoding}), QUERY={" + # rubocop:disable Style/StringConcatenation
        query.map { |k, v| "#{k} (#{k.encoding}) => #{v} (#{v.encoding})" }.join(',') +
        "}\r\n"
    end
  end
end
