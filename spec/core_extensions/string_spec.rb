# frozen_string_literal: true

require 'core_extensions/string'

RSpec.describe CoreExtensions::String do
  before do
    String.include described_class
  end

  describe '#unaccentize' do
    context 'given an empty string' do
      it 'should return an empty string' do
        tested_string = ''
        expect(tested_string.unaccentize).to eql(tested_string)
      end
    end

    context 'given the printable ASCII characters (0x20 to 0x7E)' do
      it 'should not modify them' do
        tested_string1 = ' !\"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNO'
        tested_string2 = 'PQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~'
        expect(tested_string1.unaccentize).to eql(tested_string1)
        expect(tested_string2.unaccentize).to eql(tested_string2)
      end
    end

    context 'given the Unicode characters from the Latin-1 supplement (0xA0 to 0xFF)' do
      it 'should replace the accented characters with their unaccented version' do
        tested_string1   = '¡¢£¤¥¦§¨©ª«¬®¯°±²³´µ¶·¸¹º»¼½¾¿ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐ'
        tested_string2   = 'ÑÒÓÔÕÖ×ØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõö÷øùúûüýþÿ'
        expected_string1 = '¡¢£¤¥¦§¨©ª«¬®¯°±²³´µ¶·¸¹º»¼½¾¿AAAAAAAECEEEEIIIID'
        expected_string2 = 'NOOOOO×OUUUUYÞssaaaaaaaeceeeeiiiidnooooo÷ouuuuyþy'
        expect(tested_string1.unaccentize).to eql(expected_string1)
        expect(tested_string2.unaccentize).to eql(expected_string2)
      end
    end

    context 'given the Unicode characters from the Latin Extended-A (0x100 to 0x17F)' do
      it 'should replace the accented characters with their unaccented version' do
        tested_string1   = 'ĀāĂăĄąĆćĈĉĊċČčĎďĐđĒēĔĕĖėĘęĚěĜĝĞğĠġĢģĤĥĦħĨĩĪīĬĭĮįİıĲĳĴĵĶķĸĹĺĻļĽľĿ'
        tested_string2   = 'ŀŁłŃńŅņŇňŉŊŋŌōŎŏŐőŒœŔŕŖŗŘřŚśŜŝŞşŠšŢţŤťŦŧŨũŪūŬŭŮůŰűŲųŴŵŶŷŸŹźŻżŽžſ'
        expected_string1 = 'AaAaAaCcCcCcCcDdDdEeEeEeEeEeGgGgGgGgHhHhIiIiIiIiIiIJijJjKkkLlLlLlL'
        expected_string2 = 'lLlNnNnNnnNnOoOoOoOEoeRrRrRrSsSsSsSsTtTtTtUuUuUuUuUuUuWwYyYZzZzZzs'
        expect(tested_string1.unaccentize).to eql(expected_string1)
        expect(tested_string2.unaccentize).to eql(expected_string2)
      end
    end
  end

  describe '#urlize' do
    context 'given an empty string' do
      it 'should return an empty string' do
        tested_string = ''
        expect(tested_string.urlize).to eql(tested_string)
      end
    end

    context 'given an uppercase string' do
      it 'should return the corresponding downcased string' do
        tested_string   = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
        expected_string = 'abcdefghijklmnopqrstuvwxyz'
        expect(tested_string.urlize).to eql(expected_string)
      end
    end

    context 'given a string with leading and trailing spaces' do
      it 'should return the same string with leading and trailing spaces removed' do
        tested_string   = '  abcde  '
        expected_string = 'abcde'
        expect(tested_string.urlize).to eql(expected_string)
      end
    end

    context 'given a string with ` ` and `\'` characters' do
      it 'should return a string with ` ` and `\'` both replaced by `_`' do
        tested_string   = 'a \'a'
        expected_string = 'a__a'
        expect(tested_string.urlize).to eql(expected_string)
      end
    end

    context 'given a string with `-` and `_` characters or numbers' do
      it 'should return the same string' do
        tested_string   = '-_0123456789'
        expected_string = '-_0123456789'
        expect(tested_string.urlize).to eql(expected_string)
      end
    end

    context 'given a string with special characters only' do
      it 'should return an empty string (special characters removed)' do
        tested_string   = '!"#$%&()*+,./:;<=>?@[\\]^`{|}~'
        expected_string = ''
        expect(tested_string.urlize).to eql(expected_string)
      end
    end

    context 'given a string with characters from Code page 852 (Latin-2 DOS)' do
      it 'should return the same string without accented nor special characters, downcased' do
        tested_string1   = 'ÇüéâäàåçêëèïîìÄÅÉæÆôöòûùÿÖÜø£Ø×ƒáíóúñÑªº¿®¬½¼¡«»░▒▓│┤ÁÂÀ©╣║╗╝¢¥┐'
        tested_string2   = '└┴┬├─┼ãÃ╚╔╩╦╠═╬¤ðÐÊËÈıÍÎÏ┘┌█▄¦Ì▀ÓßÔÒõÕµþÞÚÛÙýÝ¯´≡±‗¾¶§÷¸°¨·¹³²■'
        expected_string1 = 'cueaaaaceeeiiiaaeaeaeooouuyouooaiounnaaa'
        expected_string2 = 'aaddeeeiiiiiossoooouuuyy'
        expect(tested_string1.urlize).to eql(expected_string1)
        expect(tested_string2.urlize).to eql(expected_string2)
      end
    end
  end

  describe '#urlized?' do
    context 'given an empty string' do
      it 'should return true' do
        tested_string = ''
        expect(tested_string.urlized?).to be true
      end
    end

    context 'given a string containing only allowed characters' do
      it 'should return true' do
        tested_string = '-_abcdefghijklmnopqrstuvwxyz0123456789'
        expect(tested_string.urlized?).to be true
      end
    end

    context 'given a string containing at least only forbidden character' do
      it 'should return false' do
        tested_string1 = 'foo bar'
        expect(tested_string1.urlized?).to be false
        # ---
        tested_string2 = 'fooBar'
        expect(tested_string2.urlized?).to be false
      end
    end
  end

  describe '#humanize' do
    context 'given an empty string' do
      it 'should return an empty string' do
        tested_string = ''
        expect(tested_string.humanize).to eql(tested_string)
      end
    end

    context 'given a snake-case string' do
      it 'should return the corresponding string with spaces and each word capitalized' do
        tested_string   = 'this_is_a_sentence_containing_several_words'
        expected_string = 'This Is a Sentence Containing Several Words'
        expect(tested_string.humanize).to eql(expected_string)
      end
    end
  end
end
