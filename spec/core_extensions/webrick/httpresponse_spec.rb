# frozen_string_literal: true

require 'core_extensions/webrick/httpresponse'
require 'intranet/core/haml_wrapper'
require 'intranet/core/locales'

RSpec.describe CoreExtensions::WEBrick::HTTPResponse do
  before do
    Intranet::Core::Locales.initialize
    Intranet::Core::HamlWrapper.initialize
    WEBrick::HTTPResponse.include described_class
    @httpresponse = WEBrick::HTTPResponse.new(WEBrick::Config::HTTP)
  end

  describe '#create_error_page' do
    context 'with an error code' do
      before do
        @httpresponse.status = 404
        @httpresponse.create_error_page
      end
      it 'should return a customized error page for that error code' do
        expect(@httpresponse.body).to eql(
          "<!DOCTYPE html>\n<html>\n<head>\n" \
          "<title>\n#{HTMLEntities.new.encode(I18n.t('nav.error.http'), :named)} 404 | \n" \
          "#{HTMLEntities.new.encode(I18n.t('nav.error.http_404.short'), :named)}\n</title>\n" \
          "<meta charset='utf-8'>\n" \
          "<link href='/design/favicon.ico' rel='icon' type='image/x-icon'>\n" \
          "<link href='/design/style.css' rel='stylesheet' type='text/css'>\n" \
          "</head>\n<body>\n<header>\n" \
          "<h1>\n<a href='/index.html'>#{Socket.gethostname.capitalize}</a>\n</h1>\n" \
          "</header>\n<main>\n<section id='error'>\n" \
          "<h2>\n#{HTMLEntities.new.encode(I18n.t('nav.error.http'), :named)} 404\n<br>\n" \
          "#{HTMLEntities.new.encode(I18n.t('nav.error.http_404.short'), :named)}" \
          "\n</h2>\n<p>\n<img alt='' src='/design/error.png'>\n<br>\n" \
          "#{HTMLEntities.new.encode(I18n.t('nav.error.http_404.full'), :named)}\n" \
          "<br>\n<br>\n<a href='/index.html'>" \
          "#{HTMLEntities.new.encode(I18n.t('nav.back.home'), :named)}</a>\n</p>\n</section>\n" \
          "</main>\n<footer></footer>\n</body>\n</html>\n"
        )
      end
    end
  end
end
