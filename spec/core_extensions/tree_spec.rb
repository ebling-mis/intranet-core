# frozen_string_literal: true

require 'core_extensions/tree'

RSpec.describe CoreExtensions::Tree do
  describe '.initialize' do
    context 'creating a new Tree' do
      before do
        @tree = described_class.new
      end
      it 'should return a Tree with no children' do
        expect(@tree.children?).to be false
        expect(@tree.children_nodes).to be_empty
      end
      it 'should return a Tree with root value equal to nil' do
        expect(@tree.value).to be_nil
      end
    end

    context 'creating a new Tree with a root VALUE' do
      before do
        @tree = described_class.new(42)
      end
      it 'should return a Tree with no children' do
        expect(@tree.children?).to be false
        expect(@tree.children_nodes).to be_empty
      end
      it 'should return a Tree with root value equal to VALUE' do
        expect(@tree.value).to eql(42)
      end
    end
  end

  describe '#add_child_node' do
    context 'given a child ID' do
      before do
        @tree = described_class.new
        @ref = @tree.add_child_node('first')
      end
      it 'should add a new child node to the Tree, identified by ID and holding nil value' do
        expect(@tree.children?).to be true
        expect(@tree.children_nodes.size).to be_eql(1)
        expect(@tree.child_exists?('first')).to be true
        expect(@tree.child_node('first').value).to be_nil
      end
      it 'should return a reference to the newly created child node' do
        expect(@ref).to be(@tree.child_node('first'))
      end
    end

    context 'given a child ID and a VALUE' do
      before do
        @tree = described_class.new
        @ref = @tree.add_child_node('first', 43)
      end
      it 'should add a new child node to the Tree, identified by ID and holding VALUE' do
        expect(@tree.children?).to be true
        expect(@tree.children_nodes.size).to be_eql(1)
        expect(@tree.child_exists?('first')).to be true
        expect(@tree.child_node('first').value).to be_eql(43)
      end
      it 'should return a reference to the newly created child node' do
        expect(@ref).to be(@tree.child_node('first'))
      end
    end

    context 'given a child ID corresponding to an existing child' do
      before do
        @tree = described_class.new
        @ref1 = @tree.add_child_node('first', 43)
        @ref2 = @tree.add_child_node('first', 44)
      end
      it 'should left the tree unchanged' do
        expect(@tree.children?).to be true
        expect(@tree.children_nodes.size).to be_eql(1)
        expect(@tree.child_exists?('first')).to be true
        expect(@tree.child_node('first').value).to be_eql(43)
      end
      it 'should return a reference to the existing child with given ID' do
        expect(@ref2).to be(@ref1)
      end
    end
  end

  describe '#child_exists' do
    context 'given a non-existant child ID' do
      before do
        @tree = described_class.new
        @tree.add_child_node('first')
      end
      it 'should return false' do
        expect(@tree.child_exists?('second')).to be false
      end
    end
  end

  describe '#child_node' do
    context 'given a non-existant child ID' do
      before do
        @tree = described_class.new
        @tree.add_child_node('first')
      end
      it 'should raise KeyError' do
        expect { @tree.child_node('second') }.to raise_error(KeyError)
      end
    end
  end

  describe '#to_h' do
    context 'given a 10-nodes Tree' do
      before do
        @tree = described_class.new('node0')
        @tree.add_child_node('left', 'node1')
        @tree.add_child_node('middle', 'node2')
        @tree.add_child_node('right', 'node3')
        @tree.child_node('left').add_child_node('left', 'node1.1')
        @tree.child_node('left').add_child_node('right', 'node1.2')
        @tree.child_node('middle').add_child_node('middle', 'node2.1')
        @tree.child_node('right').add_child_node('left', 'node3.1')
        @tree.child_node('right').add_child_node('middle', 'node3.2')
        @tree.child_node('right').add_child_node('right', 'node3.3')
      end
      it 'should return a depth-first representation of the Tree' do
        expect(@tree.to_h).to eql(
          {
            '/' => 'node0',
            '/left' => 'node1',
            '/left/left' => 'node1.1',
            '/left/right' => 'node1.2',
            '/middle' => 'node2',
            '/middle/middle' => 'node2.1',
            '/right' => 'node3',
            '/right/left' => 'node3.1',
            '/right/middle' => 'node3.2',
            '/right/right' => 'node3.3'
          }
        )
      end
    end

    context 'given a 10-nodes Tree and a separator' do
      before do
        @tree = described_class.new(0.0)
        @tree.add_child_node('left', 1.0)
        @tree.add_child_node('middle', 2.0)
        @tree.add_child_node('right', 3.0)
        @tree.child_node('left').add_child_node('left', 1.1)
        @tree.child_node('left').add_child_node('right', 1.2)
        @tree.child_node('middle').add_child_node('middle', 2.1)
        @tree.child_node('right').add_child_node('left', 3.1)
        @tree.child_node('right').add_child_node('middle', 3.2)
        @tree.child_node('right').add_child_node('right', 3.3)
      end
      it 'should return a depth-first representation of the Tree, using the given separator' do
        expect(@tree.to_h(':')).to eql(
          {
            ':' => 0.0,
            ':left' => 1.0,
            ':left:left' => 1.1,
            ':left:right' => 1.2,
            ':middle' => 2.0,
            ':middle:middle' => 2.1,
            ':right' => 3.0,
            ':right:left' => 3.1,
            ':right:middle' => 3.2,
            ':right:right' => 3.3
          }
        )
      end
    end
  end

  describe '#to_s' do
    context 'given a Tree' do
      before do
        @tree = described_class.new(0.0)
        @tree.add_child_node('left', 1.0)
        @tree.add_child_node('middle', 2.0)
        @tree.add_child_node('right', 3.0)
        @tree.child_node('left').add_child_node('left', 1.1)
        @tree.child_node('left').add_child_node('right', 1.2)
        @tree.child_node('middle').add_child_node('middle', 2.1)
        @tree.child_node('right').add_child_node('left', 3.1)
        @tree.child_node('right').add_child_node('middle', 3.2)
        @tree.child_node('right').add_child_node('right', 3.3)
      end
      it 'should return an ASCII representation of the Tree' do
        expected_string = <<~END_HEREDOC
          VALUE: 0.0
           * ID:   'left'
             VALUE: 1.0
              * ID:   'left'
                VALUE: 1.1
              * ID:   'right'
                VALUE: 1.2
           * ID:   'middle'
             VALUE: 2.0
              * ID:   'middle'
                VALUE: 2.1
           * ID:   'right'
             VALUE: 3.0
              * ID:   'left'
                VALUE: 3.1
              * ID:   'middle'
                VALUE: 3.2
              * ID:   'right'
                VALUE: 3.3
        END_HEREDOC
        expect(@tree.to_s).to eql(expected_string)
      end
    end
  end
end
