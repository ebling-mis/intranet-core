# frozen_string_literal: true

require 'intranet/logger'

RSpec.describe Intranet::Logger do
  it 'should define 5 logging levels (FATAL, ERROR, WARN, INFO, DEBUG)' do
    expect { described_class::FATAL }.not_to raise_error
    expect { described_class::ERROR }.not_to raise_error
    expect { described_class::WARN  }.not_to raise_error
    expect { described_class::INFO  }.not_to raise_error
    expect { described_class::DEBUG }.not_to raise_error
  end

  it { is_expected.to respond_to(:fatal, :error, :warn, :info, :debug) }

  describe '#log' do
    before(:all) do
      @logger = described_class.new
    end

    test_cases = [
      {
        level: described_class::FATAL, level_str: 'FATAL', level_method: :fatal,
        displayed: {
          described_class::FATAL => true,
          described_class::ERROR => true,
          described_class::WARN => true,
          described_class::INFO => true,
          described_class::DEBUG => true
        },
        color_regex: /^\033\[1;37;41m.*\033\[0m$/,
        color_desc: 'bold white on a red background'
      },
      {
        level: described_class::ERROR, level_str: 'ERROR', level_method: :error,
        displayed: {
          described_class::FATAL => false,
          described_class::ERROR => true,
          described_class::WARN => true,
          described_class::INFO => true,
          described_class::DEBUG => true
        },
        color_regex: /^\033\[0;31m.*\033\[0m$/,
        color_desc: 'red'
      },
      {
        level: described_class::WARN, level_str: 'WARN', level_method: :warn,
        displayed: {
          described_class::FATAL => false,
          described_class::ERROR => false,
          described_class::WARN => true,
          described_class::INFO => true,
          described_class::DEBUG => true
        },
        color_regex: /^\033\[0;33m.*\033\[0m$/,
        color_desc: 'yellow'
      },
      {
        level: described_class::INFO, level_str: 'INFO', level_method: :info,
        displayed: {
          described_class::FATAL => false,
          described_class::ERROR => false,
          described_class::WARN => false,
          described_class::INFO => true,
          described_class::DEBUG => true
        },
        color_regex: /^\033\[0;36m.*\033\[0m$/,
        color_desc: 'cyan'
      },
      {
        level: described_class::DEBUG, level_str: 'DEBUG', level_method: :debug,
        displayed: {
          described_class::FATAL => false,
          described_class::ERROR => false,
          described_class::WARN => false,
          described_class::INFO => false,
          described_class::DEBUG => true
        },
        color_regex: /^\033\[0;35m.*\033\[0m$/,
        color_desc: 'purple'
      }
    ].freeze

    test_cases.each do |tc|
      context "given a #{tc[:level_str]} message" do
        it "should only be displayed if logger level is at least #{tc[:level_str]}" do
          tc[:displayed].each do |level, displayed|
            @logger.level = level
            if displayed
              expect { @logger.log(tc[:level], 'TEST_MSG') }
                .to output(/TEST_MSG/).to_stderr_from_any_process
              expect { @logger.send(tc[:level_method], 'TEST_MSG') }
                .to output(/TEST_MSG/).to_stderr_from_any_process
            else
              expect { @logger.log(tc[:level], 'TEST_MSG') }
                .not_to output.to_stderr_from_any_process
              expect { @logger.send(tc[:level_method], 'TEST_MSG') }
                .not_to output.to_stderr_from_any_process
            end
          end
        end

        it 'should be timestamped' do
          @logger.level = tc[:level]
          time_of_test = Time.now
          allow(Time).to receive(:now).and_return(time_of_test)
          expect { @logger.log(tc[:level], 'TEST_MSG') }
            .to output(/#{time_of_test.strftime('\\[%Y-%m-%d %H:%M:%S\\]')}/)
            .to_stderr_from_any_process
          expect { @logger.send(tc[:level_method], 'TEST_MSG') }
            .to output(/#{time_of_test.strftime('\\[%Y-%m-%d %H:%M:%S\\]')}/)
            .to_stderr_from_any_process
        end

        it "should be colored in #{tc[:color_desc]}" do
          @logger.level = tc[:level]
          expect { @logger.log(tc[:level], 'TEST_MSG') }
            .to output(tc[:color_regex]).to_stderr_from_any_process
          expect { @logger.send(tc[:level_method], 'TEST_MSG') }
            .to output(tc[:color_regex]).to_stderr_from_any_process
        end
      end
    end

    after(:all) do
      @logger.close
    end
  end
end
