# frozen_string_literal: true

require 'intranet/core/haml_wrapper'

RSpec.describe Intranet::Core::HamlWrapper do
  describe '.initialize' do
    before do
      @load_path = File.absolute_path('../../../lib/intranet/resources/haml', __dir__)
      described_class.initialize(@load_path)
    end

    it 'should define the default load path' do
      expect(described_class.load_path).to eql([@load_path])
    end
  end

  describe '.add_path' do
    before do
      @load_path = File.absolute_path('../../../lib/intranet/resources/haml', __dir__)
      described_class.initialize(@load_path)
      described_class.add_path(__dir__)
    end

    it 'should prepend the given directory to the HAML load path' do
      expect(described_class.load_path.first).to eql(__dir__)
    end
  end

  #---

  let(:included) { Class.new { include Intranet::Core::HamlWrapper } }
  let(:extended) { Class.new { extend Intranet::Core::HamlWrapper } }

  describe 'to_markup' do
    before do
      described_class.initialize
    end

    context 'if the template is not found in path' do
      it 'should return an empty string' do
        expect(included.new.to_markup('')).to be_empty
        expect(extended.to_markup('')).to be_empty

        expect(included.new.to_markup('unknown')).to be_empty
        expect(extended.to_markup('unknown')).to be_empty
      end
    end

    context 'given a valid HAML template and locals' do
      test_in  = { title: 'TITLE_1', nav: { 'link_1' => '/index.html', 'link_2' => nil } }
      test_out = "<h2>TITLE_1</h2>\n" \
                 "<ul class='breadcrumb'>\n" \
                 "<li>\n<a href='/index.html'>link_1</a>\n</li>\n" \
                 "<li>link_2</li>\n" \
                 "</ul>\n"
      it 'should generate the markup from template using locals value' do
        expect(included.new.to_markup('title_and_breadcrumb', test_in)).to eql(test_out)
        expect(extended.to_markup('title_and_breadcrumb', test_in)).to eql(test_out)
      end
    end
  end
end
