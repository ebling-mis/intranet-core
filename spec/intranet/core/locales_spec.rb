# frozen_string_literal: true

require 'intranet/core/locales'

RSpec.describe Intranet::Core::Locales do
  describe '.initialize' do
    before do
      @load_path = File.absolute_path('../../../lib/intranet/resources/locales', __dir__)
      described_class.initialize(@load_path)
    end

    it 'should define i18n load path' do
      Dir[File.join(@load_path, '*.yml')].each do |file|
        expect(I18n.load_path).to include(file)
      end
    end

    it 'should define i18n available locales according to the existing translation files' do
      Dir[File.join(@load_path, '*.yml')].each do |file|
        expect(I18n.available_locales).to include(File.basename(file).delete('.yml').to_sym)
      end
    end

    context 'if ENV[\'LANG\'] is not empty and contains an available locale' do
      it 'should define i18n default locale from ENV[\'LANG\'] value' do
        ENV['LANG'] = 'fr_FR.UTF-8'
        described_class.initialize(@load_path)
        expect(I18n.default_locale).to eql(:fr)

        ENV['LANG'] = 'en_GB.UTF-8'
        described_class.initialize(@load_path)
        expect(I18n.default_locale).to eql(:en)

        ENV['LANG'] = 'en_US.UTF-8'
        described_class.initialize(@load_path)
        expect(I18n.default_locale).to eql(:en)
      end
    end

    context 'if ENV[\'LANG\'] is not defined or empty' do
      it 'should define i18n default locale to :en' do
        ENV['LANG'] = nil
        described_class.initialize(@load_path)
        expect(I18n.default_locale).to eql(:en)

        ENV['LANG'] = ''
        described_class.initialize(@load_path)
        expect(I18n.default_locale).to eql(:en)
      end
    end

    context 'if ENV[\'LANG\'] contains an unknown locale' do
      it 'should define i18n default locale to :en' do
        ENV['LANG'] = 'ja_JP.UTF-8'
        described_class.initialize(@load_path)
        expect(I18n.default_locale).to eql(:en)
      end
    end
  end

  describe '.add_path' do
    before do
      load_path = File.absolute_path('../../../lib/intranet/resources/locales', __dir__)
      described_class.initialize(load_path)
      described_class.add_path(__dir__)
    end

    it 'should prepend the given directory to the i18n load path' do
      Dir[File.join(__dir__, '*.yml')].each do |file|
        expect(I18n.load_path.first).to include(file)
      end
    end
  end
end
