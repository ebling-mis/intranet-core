# frozen_string_literal: true

# This file is automatically loaded in each *_spec.rb file, so keep it light!

RSpec.configure do |config|
  config.expect_with :rspec do |expectations|
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true
  end

  config.mock_with :rspec do |mocks|
    mocks.verify_partial_doubles = true
  end

  config.shared_context_metadata_behavior = :apply_to_host_groups

  config.disable_monkey_patching!

  # config.warnings = true

  # Use the documentation formatter when RSpec is launched with one file only
  config.default_formatter = 'doc' if config.files_to_run.one?

  # Print the 10 slowest examples and example groups at the
  # end of the spec run, to help surface which specs are running
  # particularly slow.
  # config.profile_examples = 10

  # Run specs in random order to surface order dependencies. If you find an
  # order dependency and want to debug it, you can fix the order by providing
  # the seed, which is printed after each run.
  #     --seed 1234
  config.order = :random

  # Seed global randomization in this process using the `--seed` CLI option.
  # Setting this allows you to use `--seed` to deterministically reproduce
  # test failures related to randomization by passing the same `--seed` value
  # as the one that triggered the failure.
  Kernel.srand config.seed

  # Add lib/ directory to load path
  $LOAD_PATH << File.absolute_path(File.join('..', 'lib'), __dir__)

  # Load and start SimpleCov to gather code coverage information
  require 'simplecov'
  SimpleCov.start do
    enable_coverage :branch                   # measure branches coverage
    primary_coverage :branch
    minimum_coverage line: 100, branch: 100   # minimal coverage rate to succeed
    add_filter 'spec'                         # exclude 'spec' folder from coverage
  end
end
