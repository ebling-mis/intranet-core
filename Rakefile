# frozen_string_literal: true

require 'bundler/setup'
require 'rspec/core/rake_task'
require 'rubocop/rake_task'
require 'yard'

$LOAD_PATH << File.join(__dir__, 'lib')

task default: %w[rspec rubocop doc]

desc 'Run all RSpec tests'
RSpec::Core::RakeTask.new(:rspec)

desc 'Run RuboCop to detect coding rules violations'
RuboCop::RakeTask.new(:rubocop)

desc 'Generate documentation (using Yard)'
YARD::Rake::YardocTask.new(:doc) do |task|
  task.stats_options = ['--list-undoc']
end
