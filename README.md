# intranet-core

`intranet-core` provides the core component of a generic, highly customizable
intranet built around [WEBrick HTTP server](https://rubygems.org/gems/webrick).
Each section of the intranet can be provided by a different _module_, which
is basically a Webrick servlet in charge of a specific subdirectory of the
web server.

## Usage

### Creating a custom _module_

You can create a new _module_ by deriving from `Intranet::AbstractResponder`:

```ruby
require 'intranet/abstract_responder'

class MyModule << Intranet::AbstractResponder
  def initialize(params = {})
    @params = params
  end

  def generate_page(path, query)
    # generate HTML for the given path
  end
end
```

### Starting the Intranet

The Intranet is controlled by the `Intranet::Core` class.

```ruby
intranet = Intranet::Core.new
module = MyModule.new
intranet.register_module(module, 'my_module', __dir__)
intranet.start
```
