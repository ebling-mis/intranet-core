# frozen_string_literal: true

require 'htmlentities'
require 'webrick'
require_relative '../../intranet/core/haml_wrapper'

module CoreExtensions
  # @!visibility protected
  module WEBrick
    # @!visibility protected
    # Extension of +WEBrick::HTTPResponse+ to provide the hook
    # +create_error_page+.
    module HTTPResponse
      include Intranet::Core::HamlWrapper

      # Provides custom error pages for common HTTP errors.
      def create_error_page
        @body << to_markup('http_error', error: @status)
      end
    end
  end
end
