# frozen_string_literal: true

require 'titleize'

module CoreExtensions
  # Extension of Ruby's standard library +String+ class.
  module String
    # Replaces all accented characters in a string with their non-accented version.
    # @return [String]
    def unaccentize # rubocop:disable Metrics/MethodLength, Metrics/AbcSize
      tr('ÀÁÂÃÄÅàáâãäåĀāĂăĄą',       'AAAAAAaaaaaaAaAaAa')
        .tr('ÇçĆćĈĉĊċČčÐðĎďĐđ',      'CcCcCcCcCcDdDdDd')
        .tr('ÈÉÊËèéêëĒēĔĕĖėĘęĚě',    'EEEEeeeeEeEeEeEeEe')
        .tr('ĜĝĞğĠġĢģĤĥĦħ',          'GgGgGgGgHhHh')
        .tr('ÌÍÎÏìíîïĨĩĪīĬĭĮįİı',    'IIIIiiiiIiIiIiIiIi')
        .tr('ĴĵĶķĸĹĺĻļĽľĿŀŁł',       'JjKkkLlLlLlLlLl')
        .tr('ÑñŃńŅņŇňŉŊŋ',           'NnNnNnNnnNn')
        .tr('ÒÓÔÕÖØòóôõöøŌōŎŏŐő',    'OOOOOOooooooOoOoOo')
        .tr('ŔŕŖŗŘřŚśŜŝŞşŠšſŢţŤťŦŧ', 'RrRrRrSsSsSsSssTtTtTt')
        .tr('ÙÚÛÜùúûüŨũŪūŬŭŮůŰűŲų',  'UUUUuuuuUuUuUuUuUuUu')
        .tr('ŴŵÝýÿŶŷŸŹźŻżŽž',        'WwYyyYyYZzZzZz')
        .gsub(/ß/, 'ss')
        .gsub(/Æ/, 'AE')
        .gsub(/æ/, 'ae')
        .gsub(/Œ/, 'OE')
        .gsub(/œ/, 'oe')
        .gsub(/Ĳ/, 'IJ')
        .gsub(/ĳ/, 'ij')
    end

    # Converts a string to a snake-cased format suitable for URL and/or CSS attributes.
    # @return [String]
    def urlize
      strip.unaccentize.downcase.tr(' \'', '_').delete('^-_a-z0-9')
    end

    # Tests whether a string is urlize-d, ie. it is only constituted of characters suitable for URL
    # and/or CSS attributes.
    # @return [Boolean]
    def urlized?
      scan(/[^-_a-z0-9]/).empty?
    end

    # Turns underscores into spaces and titleize a string.
    # @return [String]
    def humanize
      tr('_', ' ').titleize
    end
  end
end
