# frozen_string_literal: true

require_relative 'core_extensions/string'
require_relative 'core_extensions/tree'
require_relative 'core_extensions/webrick/httpresponse'

String.include CoreExtensions::String
WEBrick::HTTPResponse.include CoreExtensions::WEBrick::HTTPResponse

# @!visibility protected
module CoreExtensions
end
