/**
 * nav.js
 * JavaScript functions for the main navigation menu (used on small screens)
 * and for the modal box.
 */

const openMenu = document.getElementById('openmenu');
const closeMenu = document.getElementById('closemenu');
const navMenu = document.querySelectorAll('header nav')[0];
openMenu.addEventListener('click', function() { navMenu.style.width = 'auto'; });
closeMenu.addEventListener('click', function() { navMenu.style.width = ''; });

const openModal = document.getElementById('openmodal');
const modal = document.getElementById('modal');
openModal.addEventListener('click', function() { modal.style.display = 'block'; });
modal.addEventListener('click', function(event) {
  if (event.target == modal) {
    modal.style.display = 'none';
  }
});
