# frozen_string_literal: true

require 'webrick'

module Intranet
  # The default logger for the Intranet. It is based on +WEBrick::BasicLog+ but adds timestamp and
  # colors to the messages.
  class Logger < WEBrick::BasicLog
    COLOR = {
      FATAL => "\033[1;37;41m",
      ERROR => "\033[0;31m",
      WARN => "\033[0;33m",
      INFO => "\033[0;36m",
      DEBUG => "\033[0;35m"
    }.freeze
    private_constant :COLOR

    # Initializes a new logger for $stderr that outputs messages at +level+ or higher.
    # +level+ can be modified later (attribute accessor).
    # @param level The initial log level.
    def initialize(level = INFO)
      super($stderr, level)
    end

    # Logs a message at a given level if it is above the current log level.
    # @param level The level of the message
    # @param msg [String] The message
    def log(level, msg)
      super(level, "#{COLOR[level]}#{Time.now.strftime('[%Y-%m-%d %H:%M:%S]')} #{msg}\033[0m")
    end

    # Logs an object that responds to +to_s+ at level INFO.
    # @param obj [Object] The object to log.
    def <<(obj)
      info(obj.to_s.chomp)
    end
  end
end
