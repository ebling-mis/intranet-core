# frozen_string_literal: true

require 'i18n'

module Intranet
  class Core
    # @!visibility protected
    # Wrapps the initialisation of the i18n internationalization gem.
    module Locales
      # The default path to the locales files.
      DEFAULT_LOCALES_DIR = File.join(__dir__, '..', 'resources', 'locales')

      # Initializes translation units.
      # @param default_locales_dir [String] The path to the directory containing default translation
      #                                     units.
      def self.initialize(default_locales_dir = DEFAULT_LOCALES_DIR)
        I18n.load_path = translation_files(default_locales_dir)
        I18n.available_locales = available_locales(default_locales_dir)
        begin
          I18n.default_locale = ENV['LANG'].split('.')[0].split('_')[0].to_sym
        rescue NoMethodError, I18n::InvalidLocale
          I18n.default_locale = :en
        end
      end

      # Adds a translation units directory.
      # @param dir [String] The path to the directory to add.
      def self.add_path(dir)
        I18n.load_path = Dir.glob(File.join(dir, '*.yml')) + I18n.load_path # prepend load_path
      end

      # private

      def self.translation_files(dir)
        Dir.glob(File.join(dir, '*.yml'))
      end
      private_class_method :translation_files

      def self.available_locales(dir)
        translation_files(dir).map do |file|
          File.basename(file, File.extname(file)).to_sym
        end
      end
      private_class_method :available_locales
    end
  end
end
