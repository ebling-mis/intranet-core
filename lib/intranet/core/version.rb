# frozen_string_literal: true

module Intranet
  class Core
    # The name of the gem.
    NAME = 'intranet-core'

    # The version of the gem, according to semantic versionning.
    VERSION = '2.5.0'

    # The URL of the gem homepage.
    HOMEPAGE_URL = 'https://rubygems.org/gems/intranet-core'

    # The URL of the gem source code.
    SOURCES_URL = 'https://bitbucket.org/ebling-mis/intranet-core'
  end
end
