# frozen_string_literal: true

module Intranet
  # The default implementation and interface of an Intranet module.
  class AbstractResponder
    # Returns the name of the module.
    # @return [String] The name of the module
    def self.module_name; end

    # The version of the module, according to semantic versionning.
    # @return [String] The version of the module
    def self.module_version; end

    # The homepage of the module, if any.
    # @return [String/Nil] The homepage URL of the module, or nil if no homepage is available.
    def self.module_homepage; end

    # Specifies if the responder instance should be displayed in the main navigation menu or not.
    # @return [Boolean] True if the responder instance should be added to the main navigation menu,
    #                   False otherwise.
    def in_menu?
      true
    end

    # Specifies the absolute path to the resources directory for that module. This directory should
    # contain three subdirectories: +haml/+, +locales/+ and +www/+.
    # This method should be redefined to overwrite this default value with the actual resources
    # directory path.
    # @return [String] The absolute path to the resources directory for the module.
    def resources_dir
      File.join(__dir__, 'resources')
    end

    # Destroys the responder instance.
    # This method gets called when server is shut down.
    def finalize
      # nothing to do
    end

    # Generates the HTML answer (HTTP return code, MIME type and body) associated to the given
    # +path+ and +query+.
    #
    # The function may return a partial content, in which case the HTTP return code must be 206 and
    # the answer body is expected to be a +Hash+ with the following keys:
    # * +:title+ (mandatory): the page title
    # * +:content+ (mandatory): the partial content
    # * +:stylesheets+ (optional): an array of the required Cascade Style Sheets (CSS) files, either
    #   absolute or relative to the module root
    # * +:scripts+ (optional): an array of hashes, each representing a <script> element to be
    #   included in the generated page. Keys are the one accepted by the HTML <script> tag. The
    #   +:src+ key may be either absolute or relative to the module root
    # @param path [String] The requested URI, relative to that module root URI
    # @param query [Hash] The URI variable/value pairs, if any
    # @return [Array<Integer, String, String> or Array<Integer, String, Hash>] The HTTP return code,
    #         the MIME type and the answer body (partial if return code is 206).
    def generate_page(path, query)
      [404, '', '']
    end

    # Provides the list of Cascade Style Sheets (CSS) dependencies for this module, either using
    # absolute or relative (from the module root) paths.
    # @deprecated Use {generate_page} partial content feature, setting +:stylesheets+ attribute of
    #             the returned body.
    # @return [Array] The list of CSS dependencies, as absolute path or relative to the module root
    #                 URL.
    def css_dependencies
      []
    end

    # Provides the list of Javascript files (JS) dependencies for this module, either using
    # absolute or relative (from the module root) paths.
    # @deprecated Use {generate_page} partial content feature, setting +:scripts+ attribute of the
    #             returned body.
    # @return [Array] The list of JS dependencies, as absolute path or relative to the module root
    #                 URL.
    def js_dependencies
      []
    end
  end
end
