# frozen_string_literal: true

require 'date'
require_relative 'lib/intranet/core/version'

Gem::Specification.new do |s|
  s.name        = Intranet::Core::NAME
  s.version     = Intranet::Core::VERSION

  s.summary     = 'Core component to build a custom intranet.'
  s.homepage    = Intranet::Core::HOMEPAGE_URL
  s.metadata    = { 'source_code_uri' => Intranet::Core::SOURCES_URL }
  s.license     = 'MIT'

  s.author      = 'Ebling Mis'
  s.email       = 'ebling.mis@protonmail.com'

  # Make sure the gem is built from versioned files
  s.files         = `git ls-files -z`.split("\0").grep(/^spec|^lib|^README/)
  s.require_paths = %w[lib]

  s.required_ruby_version = '~> 3.0'

  s.add_runtime_dependency 'haml',          '~> 6.0'
  s.add_runtime_dependency 'htmlentities',  '~> 4.0'
  s.add_runtime_dependency 'i18n',          '~> 1.0'
  s.add_runtime_dependency 'titleize',      '~> 1.0'
  s.add_runtime_dependency 'webrick',       '~> 1.0'
end
